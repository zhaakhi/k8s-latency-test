#!/usr/bin/env python3
"""
Simulate some number of read-only memcached clients and log slow requests
"""
from gevent import monkey
monkey.patch_all()

from time import sleep, monotonic
import argparse
import logging
import sys

from pymemcache.client import Client
import gevent

RUNNING = True

# Millisecond resolution histogram
TIMES = [0] * 1000


def record_time(request_time):
    time_ms = int(request_time * 1000)
    # Trust the GIL
    TIMES[max(0, min(time_ms, len(TIMES)-1))] += 1


def client_loop(client: Client, client_num, args):
    while RUNNING:
        try:
            start_time = monotonic()
            client.get(b"dummy_key")
            end_time = monotonic()

            record_time(end_time - start_time)
            if end_time - start_time > args.log_threshold:
                elapsed_ms = (end_time - start_time) * 1000
                logging.warning(f"Slow get from client {client_num} took {elapsed_ms:.2f} ms")

        except Exception as e:
            elapsed_ms = (monotonic() - start_time) * 1000
            logging.warning(f"Exception from client {client_num} after {elapsed_ms:.2f} ms: {e}")
            sleep(args.error_sleep)
        sleep(args.request_interval_ms / 1000)


def make_client(server_addr, timeout: float) -> Client:
    return Client(server=server_addr, connect_timeout=1.0, timeout=timeout, no_delay=True)


def print_stats():
    times = TIMES.copy()
    lines = [f'{ms: 4}: {count:8}' for ms, count in enumerate(times)]
    logging.info("Request time distribution:\n" + '\n'.join(lines))

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--clients", type=int, default=2)
    parser.add_argument("--server", default='127.0.0.1')
    parser.add_argument("--server-port", type=int, default='11211')
    parser.add_argument('--request-interval-ms', type=float, default=1.0, help="milliseconds each client will wait between requests")
    parser.add_argument('--error-sleep', type=float, default=1.0, help="seconds to sleep after a failed request")
    parser.add_argument('--log-threshold', type=float, default=0.5, help="Latency threshold in seconds before request is logged")
    parser.add_argument('--timeout', type=float, default=1, help="Request timeout in seconds")
    parser.add_argument('--stats-interval', type=float, default=60*5, help="Seconds between stat dumps")
    parser.add_argument('--log-file', help="Write logs to file, default to stderr")

    args = parser.parse_args()

    logging.basicConfig(format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s', level=logging.INFO, filename=args.log_file)

    clients = [make_client((args.server, args.server_port), args.timeout) for _ in range(args.clients)]

    # Start threads
    logging.info(f"Spawning {len(clients)} clients")
    for i, client in enumerate(clients):
        gevent.spawn(client_loop, client=client, client_num=i, args=args)

    while True:
        sleep(args.stats_interval)
        print_stats()
        # Clear stats
        for i in range(len(TIMES)):
            TIMES[i] = 0


if __name__ == '__main__':
    main()
